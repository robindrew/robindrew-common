package com.robindrew.common.text;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.common.base.Throwables;
import com.robindrew.common.text.selection.Selection;
import com.robindrew.common.text.selection.SelectionOption;

/**
 * A utility that brings together common text processing functionality.
 */
public class Strings {

	/** The pattern cache. */
	private static final ConcurrentMap<String, Pattern> patternCache = new ConcurrentHashMap<String, Pattern>();

	/**
	 * Utility class - private constructor.
	 */
	private Strings() {
	}

	/**
	 * Returns a string representation of the given object.
	 * @param object the object.
	 * @return the string representation.
	 */
	public static String toString(Object object) {
		return ToStringBuilder.reflectionToString(object, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	/**
	 * Returns the given throwable as a stack trace.
	 * @param t the throwable.
	 * @return the stack trace.
	 */
	public static String getStackTrace(Throwable t) {
		return Throwables.getStackTraceAsString(t);
	}

	/**
	 * Returns an instance of the given pattern.
	 * @param regex the regular expression.
	 * @return the pattern.
	 */
	public static final Pattern getPattern(String regex) {
		Pattern pattern = patternCache.get(regex);
		if (pattern == null) {
			regex = regex.intern();
			pattern = Pattern.compile(regex);
			patternCache.put(regex, pattern);
		}
		return pattern;
	}

	/**
	 * Returns true if the given regular expression matches the text.
	 * @param regex the regular expression.
	 * @param text the text.
	 * @return true if the given regular expression matches the text.
	 */
	public static final boolean matches(String regex, String text) {
		Pattern pattern = getPattern(regex);
		return pattern.matcher(text).matches();
	}

	/**
	 * Returns the string value of the given object or the default if the value is null.
	 * @param object the object.
	 * @param nullDefault the default value to return if the object is null.
	 * @return the resulting string.
	 */
	public static final String valueOf(Object object, String nullDefault) {
		return Objects.toString(object, nullDefault);
	}

	/**
	 * Returns the index of the given needle in the haystack, ignoring case.
	 * @param haystack the haystack to search.
	 * @param needle the needle.
	 * @param index the start index.
	 * @return the index.
	 */
	public static int indexOfIgnoreCase(CharSequence haystack, String needle, int index) {
		if (needle.length() == 0) {
			return index;
		}
		if (needle.length() > haystack.length()) {
			return -1;
		}
		int subIndex = 0;
		for (int i = index; i < haystack.length(); i++) {
			char c1 = Character.toLowerCase(haystack.charAt(i));
			char c2 = Character.toLowerCase(needle.charAt(subIndex));
			if (c1 == c2) {
				subIndex++;
				if (subIndex == needle.length()) {
					return i - subIndex + 1;
				}
			} else {
				subIndex = 0;
			}
		}
		return -1;
	}

	/**
	 * Extract a portion of text from the given string.
	 * @param text the text to extract from.
	 */
	public static String extract(String text, String begin, String end) {
		return new Selection(text).extract(begin, end);
	}

	/**
	 * Extract a portion of text from the given string.
	 * @param text the text to extract from.
	 */
	public static String extract(String text, char begin, char end) {
		return extract(text, String.valueOf(begin), String.valueOf(end));
	}

	/**
	 * Extract a portion of text from the given string.
	 * @param text the text to extract from.
	 */
	public static String extract(String text, String begin, String end, SelectionOption... options) {
		return new Selection(text).extract(begin, end, options);
	}

	/**
	 * Extract a portion of text from the given string.
	 * @param text the text to extract from.
	 */
	public static String extract(String text, char begin, char end, SelectionOption... options) {
		return extract(text, String.valueOf(begin), String.valueOf(end), options);
	}

}
