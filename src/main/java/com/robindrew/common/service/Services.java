package com.robindrew.common.service;

import com.robindrew.common.properties.map.type.IPropertyType;
import com.robindrew.common.properties.map.type.IntegerProperty;
import com.robindrew.common.properties.map.type.StringProperty;

/**
 * The services utility class.
 */
public class Services {

	/** The service name. */
	private static final IPropertyType<String> serviceName = new StringProperty("service.name");
	/** The service port. */
	private static final IPropertyType<Integer> servicePort = new IntegerProperty("service.port");
	/** The service instance. */
	private static final IPropertyType<Integer> serviceInstance = new IntegerProperty("service.instance");

	private Services() {
		// Utility class - private constructor.
	}

	/**
	 * Returns the service name.
	 */
	public static final String getServiceName() {
		return serviceName.get();
	}

	/**
	 * Returns the service port.
	 */
	public static int getServicePort() {
		return servicePort.get();
	}

	/**
	 * Returns the service instance.
	 */
	public static int getServiceInstance() {
		return serviceInstance.get();
	}
}
