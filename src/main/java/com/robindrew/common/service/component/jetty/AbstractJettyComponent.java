package com.robindrew.common.service.component.jetty;

import java.util.concurrent.ExecutorService;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.util.thread.ExecutorThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.robindrew.common.properties.map.type.IPropertyType;
import com.robindrew.common.properties.map.type.IntegerProperty;
import com.robindrew.common.properties.map.type.StringProperty;
import com.robindrew.common.service.Services;
import com.robindrew.common.service.component.AbstractIdleComponent;
import com.robindrew.common.util.Threads;

public abstract class AbstractJettyComponent extends AbstractIdleComponent {

	private static final Logger log = LoggerFactory.getLogger(AbstractJettyComponent.class);

	/** The property "jetty.threads". */
	private static final IPropertyType<Integer> jettyThreads = new IntegerProperty("jetty.threads").defaultValue(20);
	/** The property "jetty.port". */
	private static final IPropertyType<Integer> jettyPort = new IntegerProperty("jetty.port");
	/** The property "jetty.host". */
	private static final IPropertyType<String> jettyHost = new StringProperty("jetty.host");

	@Override
	protected void startupComponent() throws Exception {

		// Create the thread pool
		int threads = jettyThreads.get();
		log.info("[Jetty] threads: {}", threads);

		ExecutorService threadPool = Threads.newFixedThreadPool("JettyComponent", threads);

		// Create the jetty server
		Server server = new Server(new ExecutorThreadPool(threadPool));

		// Create the connector for the server
		ServerConnector connector = createConnector(server);

		// Create the handler for the server
		Handler handler = createHandler();

		// Start the server
		server.addConnector(connector);
		server.setHandler(handler);
		server.setStopAtShutdown(true);
		server.start();
	}

	protected abstract Handler createHandler();

	protected ServerConnector createConnector(Server server) {

		// Create the connector
		ServerConnector connector = new ServerConnector(server);

		// The port defaults to the primary service port
		int port = jettyPort.get(Services.getServicePort());
		log.info("[Jetty] port: {}", port);
		connector.setPort(port);

		// The host is not set by default
		String host = jettyHost.get(null);
		if (host != null) {
			log.info("[Jetty] Host: {}", host);
			connector.setHost(host);
		}

		return connector;
	}

	@Override
	protected void shutdownComponent() throws Exception {
	}

}
