package com.robindrew.common.service.component.jetty;

import org.eclipse.jetty.server.Handler;

import com.google.common.base.Supplier;
import com.robindrew.common.lang.IReference;
import com.robindrew.common.lang.Reference;
import com.robindrew.common.service.component.jetty.handler.MatcherHttpHandler;
import com.robindrew.common.template.ITemplateLocator;

public abstract class JettyVelocityComponent extends AbstractJettyComponent {

	private volatile IReference<ITemplateLocator> locator = new Reference<>();

	@Override
	protected void startupComponent() throws Exception {

		// Initialise the locator
		locator.set(getTemplateLocator().get());

		// Startup Jetty
		super.startupComponent();
	}

	@Override
	protected Handler createHandler() {
		MatcherHttpHandler handler = new MatcherHttpHandler();
		populate(handler);
		return handler;
	}

	public ITemplateLocator getLocator() {
		return locator.get();
	}

	protected abstract Supplier<ITemplateLocator> getTemplateLocator();

	protected abstract void populate(MatcherHttpHandler handler);

}
