package com.robindrew.common.service.component.jetty.handler;

import static com.robindrew.common.text.Strings.getStackTrace;
import static com.robindrew.common.util.Check.notNull;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.robindrew.common.http.HttpMethod;
import com.robindrew.common.http.exception.HttpResponseException;
import com.robindrew.common.http.servlet.HttpRequest;
import com.robindrew.common.http.servlet.HttpResponse;
import com.robindrew.common.http.servlet.IHttpRequest;
import com.robindrew.common.http.servlet.IHttpResponse;
import com.robindrew.common.http.servlet.executor.IHttpExecutor;
import com.robindrew.common.http.servlet.executor.ResourceDirectoryExecutor;
import com.robindrew.common.http.servlet.matcher.CompositeMatcher;
import com.robindrew.common.http.servlet.matcher.DomainMatcher;
import com.robindrew.common.http.servlet.matcher.DomainRegexMatcher;
import com.robindrew.common.http.servlet.matcher.IHttpRequestMatcher;
import com.robindrew.common.http.servlet.matcher.MethodMatcher;
import com.robindrew.common.http.servlet.matcher.UriMatcher;
import com.robindrew.common.http.servlet.matcher.UriRegexMatcher;
import com.robindrew.common.util.Check;

public class MatcherHttpHandler extends AbstractHandler {

	private static final Logger log = LoggerFactory.getLogger(MatcherHttpHandler.class);

	private final Map<IHttpRequestMatcher, IHttpExecutor> matcherToHandlerMap = new ConcurrentHashMap<>();

	/**
	 * Register a handler to handle HTTP requests.
	 */
	public void register(IHttpRequestMatcher matcher, IHttpExecutor handler) {
		Check.notNull("matcher", matcher);
		Check.notNull("handler", handler);

		matcherToHandlerMap.put(matcher, handler);
		log.info("[Register] {} -> {}", matcher, handler);
	}

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		IHttpRequest httpRequest = new HttpRequest(request);
		IHttpResponse httpResponse = new HttpResponse(response);

		// Lookup the handler
		IHttpExecutor handler = getHandler(httpRequest);
		if (handler == null) {
			httpResponse.notFound();
			return;
		}

		// Handle the request/response
		try {
			handler.execute(httpRequest, httpResponse);
		} catch (HttpResponseException hre) {
			hre.populate(httpResponse);
		} catch (Throwable t) {
			httpResponse.internalServerError(getStackTrace(t));
		}
	}

	protected IHttpExecutor getHandler(IHttpRequest request) {
		for (Entry<IHttpRequestMatcher, IHttpExecutor> entry : matcherToHandlerMap.entrySet()) {
			IHttpRequestMatcher matcher = entry.getKey();
			if (matcher.matches(request)) {
				return entry.getValue();
			}
		}
		return null;
	}

	public ExecutorBuilder register() {
		return new ExecutorBuilder();
	}

	public class ExecutorBuilder {

		private final Set<IHttpRequestMatcher> matcherSet = new LinkedHashSet<>();

		private ExecutorBuilder matcher(IHttpRequestMatcher matcher) {
			matcherSet.add(notNull("matcher", matcher));
			return this;
		}

		public void resourceDirectory(String directory) {
			executor(new ResourceDirectoryExecutor(directory));
		}

		public void executor(IHttpExecutor executor) {
			notNull("executor", executor);

			IHttpRequestMatcher matcher = CompositeMatcher.of(matcherSet);
			register(matcher, executor);
		}

		public ExecutorBuilder method(String method) {
			return matcher(new MethodMatcher(method));
		}

		public ExecutorBuilder method(HttpMethod method) {
			return matcher(new MethodMatcher(method));
		}

		public ExecutorBuilder domain(String domain) {
			return matcher(new DomainMatcher(domain));
		}

		public ExecutorBuilder domainPattern(String domainRegex) {
			return matcher(new DomainRegexMatcher(domainRegex));
		}

		public ExecutorBuilder uri(String uri) {
			return matcher(new UriMatcher(uri));
		}

		public ExecutorBuilder uriPattern(String uriRegex) {
			return matcher(new UriRegexMatcher(uriRegex));
		}

	}
}
