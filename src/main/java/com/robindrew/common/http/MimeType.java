package com.robindrew.common.http;

import com.robindrew.common.util.Check;

public class MimeType {

	public static final MimeType TEXT_PLAIN = new MimeType("text/plain", true);
	public static final MimeType TEXT_HTML = new MimeType("text/html", true);
	public static final MimeType TEXT_CSS = new MimeType("text/css", true);
	public static final MimeType TEXT_XML = new MimeType("text/xml", true);
	public static final MimeType TEXT_CSV = new MimeType("text/csv", true);
	public static final MimeType TEXT_RTF = new MimeType("text/rtf", true);
	public static final MimeType IMAGE_ICON = new MimeType("image/x-icon", false);

	public static final MimeType APPLICATION_JSON = new MimeType("application/json", true);
	public static final MimeType APPLICATION_JAVASCRIPT = new MimeType("application/javascript", true);

	public static MimeType withExtension(String extension) {

		// Text types
		if (extension.equals("txt")) {
			return TEXT_PLAIN;
		}
		if (extension.equals("html") || extension.endsWith("htm")) {
			return TEXT_HTML;
		}
		if (extension.equals("xml")) {
			return TEXT_XML;
		}
		if (extension.equals("css")) {
			return TEXT_CSS;
		}
		if (extension.equals("csv")) {
			return TEXT_CSV;
		}
		if (extension.equals("rtf")) {
			return TEXT_RTF;
		}

		// Application types
		if (extension.equals("json")) {
			return APPLICATION_JSON;
		}
		if (extension.equals("js") || extension.equals("javascript")) {
			return APPLICATION_JAVASCRIPT;
		}

		// Image types
		if (extension.equals("ico")) {
			return IMAGE_ICON;
		}

		// Unknown
		throw new IllegalArgumentException("Unknown extension: '" + extension + "'");
	}

	private final String type;
	private final boolean text;

	public MimeType(String type, boolean text) {
		this.type = Check.notEmpty("type", type);
		this.text = text;
	}

	public String getType() {
		return type;
	}

	public boolean isText() {
		return text;
	}

	public boolean isBinary() {
		return !text;
	}

	@Override
	public String toString() {
		return type;
	}

}
