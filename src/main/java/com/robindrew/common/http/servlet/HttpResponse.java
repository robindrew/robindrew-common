package com.robindrew.common.http.servlet;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import com.google.common.io.ByteSource;
import com.robindrew.common.http.ContentType;

public class HttpResponse extends HttpServletResponseWrapper implements IHttpResponse {

	public HttpResponse(HttpServletResponse response) {
		super(response);
	}

	@Override
	public HttpServletResponse getResponse() {
		return (HttpServletResponse) super.getResponse();
	}

	@Override
	public void notFound() {
		Servlets.notFound(getResponse());
	}

	@Override
	public void internalServerError(String text) {
		Servlets.internalServerError(getResponse(), text);
	}

	@Override
	public void ok(String text, ContentType contentType) {
		Servlets.ok(getResponse(), text, contentType);
	}

	@Override
	public void ok(ByteSource source, ContentType contentType) {
		Servlets.ok(getResponse(), source, contentType);
	}
}
