package com.robindrew.common.http.servlet.executor;

import com.robindrew.common.http.servlet.IHttpRequest;
import com.robindrew.common.http.servlet.IHttpResponse;

public interface IHttpExecutor {

	void execute(IHttpRequest request, IHttpResponse response);

}
