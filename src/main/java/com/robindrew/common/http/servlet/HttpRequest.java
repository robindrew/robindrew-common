package com.robindrew.common.http.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class HttpRequest extends HttpServletRequestWrapper implements IHttpRequest {

	public HttpRequest(HttpServletRequest request) {
		super(request);
	}

	@Override
	public HttpServletRequest getRequest() {
		return (HttpServletRequest) super.getRequest();
	}

}
