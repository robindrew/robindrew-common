package com.robindrew.common.http.servlet.executor;

import java.util.LinkedHashMap;
import java.util.Map;

import com.robindrew.common.http.ContentType;
import com.robindrew.common.http.servlet.IHttpRequest;
import com.robindrew.common.http.servlet.IHttpResponse;
import com.robindrew.common.template.ITemplate;
import com.robindrew.common.template.ITemplateLocator;
import com.robindrew.common.template.TemplateData;
import com.robindrew.common.util.Check;

public abstract class VelocityHttpExecutor implements IHttpExecutor {

	private final ITemplateLocator locator;
	private final String templateName;

	public VelocityHttpExecutor(ITemplateLocator locator, String templateName) {
		this.locator = Check.notNull("locator", locator);
		this.templateName = Check.notEmpty("templateName", templateName);
	}

	@Override
	public void execute(IHttpRequest request, IHttpResponse response) {

		// Find the template
		ITemplate template = locator.getTemplate(templateName);

		// Handle the request
		Map<String, Object> dataMap = new LinkedHashMap<>();
		execute(request, response, dataMap);

		// Execute the template
		String text = template.execute(new TemplateData(dataMap));
		response.ok(text, getContentType());
	}

	/**
	 * Returns the content type. The default is HTML, override this method to change this.
	 */
	public ContentType getContentType() {
		return ContentType.TEXT_HTML;
	}

	protected abstract void execute(IHttpRequest request, IHttpResponse response, Map<String, Object> dataMap);

}
