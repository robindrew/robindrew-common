package com.robindrew.common.http.servlet;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import com.robindrew.common.http.ContentType;
import com.robindrew.common.util.Check;
import com.robindrew.common.util.Java;

/**
 * The servlets utility.
 */
public class Servlets {

	/**
	 * HTTP Response: 404 / Not Found
	 */
	public static final void notFound(HttpServletResponse response) {
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
	}

	/**
	 * HTTP Response: 500 / Internal Server Error
	 */
	public static final void internalServerError(HttpServletResponse response, String text) {
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		setContent(response, text, ContentType.TEXT_PLAIN);
	}

	/**
	 * HTTP Response: 200 / OK
	 */
	public static final void ok(HttpServletResponse response, String text, ContentType contentType) {
		response.setStatus(HttpServletResponse.SC_OK);
		setContent(response, text, contentType);
	}

	/**
	 * HTTP Response: 200 / OK
	 */
	public static final void ok(HttpServletResponse response, ByteSource source, ContentType contentType) {
		response.setStatus(HttpServletResponse.SC_OK);
		setContent(response, source, contentType);
	}

	public static void setContent(HttpServletResponse response, String text, ContentType contentType) {
		byte[] bytes = text.getBytes(Charsets.UTF_8);
		setContent(response, bytes, contentType);
	}

	public static void setContent(HttpServletResponse response, byte[] bytes, ContentType contentType) {
		Check.notNull("contentType", contentType);

		response.setContentType(contentType.toString());
		writeContent(response, bytes);
	}

	public static void setContent(HttpServletResponse response, ByteSource source, ContentType contentType) {
		Check.notNull("contentType", contentType);

		response.setContentType(contentType.toString());
		writeContent(response, source);
	}

	public static void writeContent(HttpServletResponse response, byte[] bytes) {
		try {
			ServletOutputStream output = response.getOutputStream();
			output.write(bytes);
			output.flush();
		} catch (IOException e) {
			throw Java.propagate(e);
		}
	}

	public static void writeContent(HttpServletResponse response, ByteSource source) {
		try {
			ServletOutputStream output = response.getOutputStream();
			source.copyTo(output);
			output.flush();
		} catch (IOException e) {
			throw Java.propagate(e);
		}
	}
}
