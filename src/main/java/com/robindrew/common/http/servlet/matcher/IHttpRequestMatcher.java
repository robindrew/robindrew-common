package com.robindrew.common.http.servlet.matcher;

import com.robindrew.common.http.servlet.IHttpRequest;

public interface IHttpRequestMatcher {

	boolean matches(IHttpRequest request);

}
