package com.robindrew.common.http.servlet;

import javax.servlet.http.HttpServletResponse;

import com.google.common.io.ByteSource;
import com.robindrew.common.http.ContentType;

public interface IHttpResponse extends HttpServletResponse {

	void notFound();

	void internalServerError(String text);

	void ok(String text, ContentType contentType);

	void ok(ByteSource source, ContentType contentType);

}
