package com.robindrew.common.http.exception;

import java.io.IOException;

import com.robindrew.common.http.servlet.IHttpResponse;
import com.robindrew.common.util.Check;
import com.robindrew.common.util.Java;

/**
 * Exception thrown to redirect an HTTP request to an alternative location.
 */
public class HttpRedirectException extends HttpResponseException {

	private static final long serialVersionUID = 702736571997766911L;

	private final String location;

	public HttpRedirectException(String location) {
		this.location = Check.notEmpty("location", location);
	}

	@Override
	public void populate(IHttpResponse response) {
		try {
			response.sendRedirect(location);
		} catch (IOException e) {
			Java.propagate(e);
		}
	}

}
