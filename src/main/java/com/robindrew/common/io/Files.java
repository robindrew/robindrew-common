package com.robindrew.common.io;

import static com.google.common.base.Charsets.UTF_8;

import java.io.File;

import com.google.common.io.ByteSource;
import com.google.common.io.CharSource;

public class Files {

	public static final ByteSource asByteSource(String fileName) {
		File file = new File(fileName);
		return asByteSource(file);
	}

	public static final ByteSource asByteSource(File file) {
		return com.google.common.io.Files.asByteSource(file);
	}

	public static final CharSource asCharSource(String fileName) {
		File file = new File(fileName);
		return asCharSource(file);
	}

	public static final CharSource asCharSource(File file) {
		return com.google.common.io.Files.asCharSource(file, UTF_8);
	}

}
