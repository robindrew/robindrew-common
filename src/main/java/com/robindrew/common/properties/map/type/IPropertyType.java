package com.robindrew.common.properties.map.type;

import com.google.common.base.Supplier;

public interface IPropertyType<V> extends Supplier<V> {

	boolean hasValue(V value);

	boolean exists();

	IPropertyType<V> optional();

	String getString();

	V get(V defaultValue);

}
