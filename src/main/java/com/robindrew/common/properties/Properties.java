package com.robindrew.common.properties;

import java.io.File;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.google.common.io.ByteSource;
import com.robindrew.common.io.Files;
import com.robindrew.common.io.Resources;
import com.robindrew.common.util.Java;

public class Properties {

	public static final Map<String, String> toMap(java.util.Properties properties) {
		Map<String, String> map = new TreeMap<>();
		for (Entry<Object, Object> entry : properties.entrySet()) {
			Object key = entry.getKey();
			Object value = entry.getValue();
			map.put((String) key, (String) value);
		}
		return map;
	}

	public static final Map<String, String> readFromFile(String filename) {
		return readFromFile(new File(filename));
	}

	public static final Map<String, String> readFromFile(File file) {
		boolean xml = file.getName().toLowerCase().endsWith(".xml");
		ByteSource source = Files.asByteSource(file);
		return readFromSource(source, xml);
	}

	public static final Map<String, String> readFromResource(String resourceName) {
		boolean xml = resourceName.toLowerCase().endsWith(".xml");
		ByteSource source = Resources.toByteSource(resourceName);
		return readFromSource(source, xml);
	}

	public static final Map<String, String> readFromSource(ByteSource source, boolean xml) {
		try (InputStream input = source.openBufferedStream()) {

			java.util.Properties properties = new java.util.Properties();
			if (xml) {
				properties.loadFromXML(input);
			} else {
				properties.load(input);
			}
			return toMap(properties);

		} catch (Exception e) {
			throw Java.propagate(e);
		}
	}

}
