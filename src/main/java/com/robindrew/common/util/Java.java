package com.robindrew.common.util;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;

import com.google.common.base.Throwables;

/**
 * A utility that brings together common core language functionality in to a single class.
 */
public class Java {

	/**
	 * Utility class - private constructor.
	 */
	private Java() {
	}

	/**
	 * Returns the start time of the JVM process.
	 * @return the start time of the JVM process.
	 */
	public static long getStartTime() {
		return ManagementFactory.getRuntimeMXBean().getStartTime();
	}

	/**
	 * Returns the uptime of the JVM process.
	 * @return the uptime of the JVM process.
	 */
	public static long getUptime() {
		return ManagementFactory.getRuntimeMXBean().getUptime();
	}

	/**
	 * Returns an instant in time in nanoseconds.
	 * @return an instant in time in nanoseconds.
	 */
	public static long nanoTime() {
		return System.nanoTime();
	}

	/**
	 * Returns the current time in milliseconds.
	 * @return the current time in milliseconds.
	 */
	public static final long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	/**
	 * Returns the current time in seconds.
	 * @return the current time in seconds.
	 */
	public static final int currentTimeSeconds() {
		return (int) (System.currentTimeMillis() / 1000);
	}

	/**
	 * Returns the maximum system memory the JVM can allocate.
	 * @return the maximum system memory the JVM can allocate.
	 */
	public static long maxMemory() {
		return Runtime.getRuntime().maxMemory();
	}

	/**
	 * Returns the current system memory the JVM has allocated.
	 * @return the current system memory the JVM has allocated.
	 */
	public static long totalMemory() {
		return Runtime.getRuntime().totalMemory();
	}

	/**
	 * Returns the free memory within the JVM.
	 * @return the free memory within the JVM.
	 */
	public static long freeMemory() {
		return Runtime.getRuntime().freeMemory();
	}

	/**
	 * Returns the used memory within the JVM.
	 * @return the used memory within the JVM.
	 */
	public static long usedMemory() {
		Runtime runtime = Runtime.getRuntime();
		return runtime.totalMemory() - runtime.freeMemory();
	}

	/**
	 * Propagate the given throwable, converting any checked exception to a {@link RuntimeException}.
	 * @param t the throwable to propagate.
	 * @return nothing, a {@link RuntimeException} or {@link Error} is thrown.
	 */
	public static RuntimeException propagate(Throwable t) {
		Throwables.throwIfUnchecked(t);
		throw new RuntimeException(t);
	}

	/**
	 * Terminate the JVM. This method will block indefinitely.
	 * @see java.lang.Runtime#exit(int)
	 */
	public static void exit(int status) {
		Runtime.getRuntime().exit(status);
	}

	/**
	 * Terminate the JVM. This method is non-blocking.
	 * @see java.lang.Runtime#exit(int)
	 */
	public static void exitAsync(int status) {
		SystemExit.exitAsync(status);
	}

	/**
	 * Returns the working directory of this JVM process.
	 * @return the working directory of this JVM process.
	 */
	public static String getWorkingDirectory() {
		return SystemProperties.getWorkingDirectory();
	}

	/**
	 * Returns the process id of this JVM process.
	 * @return the process id of this JVM process.
	 */
	public static long getProcessId() {
		String name = ManagementFactory.getRuntimeMXBean().getName();
		int index = name.indexOf('@');
		return Long.parseLong(name.substring(0, index));
	}

	/**
	 * Returns the (primary) address of this JVM process.
	 * @return the (primary) address of this JVM process.
	 */
	public static InetAddress getLocalHost() {
		try {
			return InetAddress.getLocalHost();
		} catch (Exception e) {
			throw propagate(e);
		}
	}

	/**
	 * Returns the (primary) hostname of this JVM process.
	 * @return the (primary) hostname of this JVM process.
	 */
	public static String getHostName() {
		return getLocalHost().getHostName();
	}

	/**
	 * Returns the (primary) host address of this JVM process.
	 * @return the (primary) host address of this JVM process.
	 */
	public static String getHostAddress() {
		return getLocalHost().getHostAddress();
	}

}
