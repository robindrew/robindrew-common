package com.robindrew.common.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * A utility that brings together common threading functionality in to a single class.
 */
public class Threads {

	/**
	 * Utility class - private constructor.
	 */
	private Threads() {
	}

	/**
	 * Sleep for the given number of milliseconds.
	 * @param millis the milliseconds.
	 */
	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw Java.propagate(e);
		}
	}

	/**
	 * Add the given shutdown hook thread.
	 * @see java.lang.Runtime#addShutdownHook(Thread)
	 */
	public static void addShutdownHook(Thread hook) {
		Check.notNull("hook", hook);
		Runtime.getRuntime().addShutdownHook(hook);
	}

	/**
	 * Creates a new fixed thread pool.
	 * @param threadNameFormat the name format for all pooled threads.
	 * @param threadCount the number of threads.
	 */
	public static ExecutorService newFixedThreadPool(String threadNameFormat, int threadCount) {
		ThreadFactory factory = newThreadFactory(threadNameFormat, true);
		return Executors.newFixedThreadPool(threadCount, factory);
	}

	/**
	 * Creates a new thread factory.
	 * @param threadNameFormat the name format for created threads.
	 * @param daemon true to indicate the threads should all be daemon threads.
	 */
	public static ThreadFactory newThreadFactory(String threadNameFormat, boolean daemon) {
		ThreadFactoryBuilder builder = new ThreadFactoryBuilder();
		builder.setNameFormat(threadNameFormat);
		builder.setDaemon(daemon);
		return builder.build();
	}
}
